# CurrencyConverter

## Dependencies
- Moya https://moya.github.io
- RxSwift https://github.com/ReactiveX/RxSwift
- SnapKit http://snapkit.io
- RxDataSources https://github.com/RxSwiftCommunity/RxDataSources
- RxSwiftExt https://github.com/RxSwiftCommunity/RxSwiftExt
- Then https://github.com/devxoul/Then

## Architecture
It's based on Redux https://redux.js.org/introduction/core-concepts