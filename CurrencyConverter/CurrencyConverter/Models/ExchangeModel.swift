import Foundation
import RxSwift
import Moya

enum ExchangeCurrency: String, Codable {
    case usd = "USD", gbp = "GBP", eur = "EUR", jpy = "JPY", brl = "BRL"
}

struct Exchange: Codable {
    var base: ExchangeCurrency
    var date: String
    var rates: [String: Float]
}

protocol ExchangeModelAPI {
    func getExchange(currency: ExchangeCurrency) -> Observable<Exchange>
}

class ExchangeModel {

    let api: ExchangeModelAPI

    init(api: ExchangeModelAPI) {
        self.api = api
    }

    func get(from currency: ExchangeCurrency) -> Observable<Exchange> {
        return self.api.getExchange(currency: currency)
    }
}
