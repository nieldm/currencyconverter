import Foundation
import Moya
import RxSwift

enum FixerAPI {
    case latest(currency: ExchangeCurrency, symbols: [ExchangeCurrency])
}

extension FixerAPI: TargetType {
    var baseURL: URL {
        return URL(string:"http://api.fixer.io")!
    }

    var sampleData: Data {
        switch self {
        case .latest:
            let jsonString = """
            {
                "base": "USD",
                "date": "2018-05-22",
                "rates": {
                    "BRL": 3.0
                }
            }
            """
            return jsonString.data(using: String.Encoding.utf8)!
        }
    }

    var task: Task {
        switch self {
        case .latest(let currency, _):
            var parameters = [String: Any]()
            parameters["base"] = currency.rawValue
            return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
        }
    }

    var headers: [String : String]? {
        return nil
    }

    var path: String {
        switch self {
        case .latest:
            return "/latest"
        }
    }

    var method: Moya.Method {
        switch self {
        case .latest:
            return .get
        }
    }

    var parameters: [String: Any]? {
        return nil
    }

    var parameterEncoding: ParameterEncoding {
        return JSONEncoding.default
    }
}

let exchangeModelFixerAPI = ExchangeModelFixerAPI()

class ExchangeModelFixerAPI: ExchangeModelAPI {

    var provider: MoyaProvider<FixerAPI>

    init() {
        self.provider = MoyaProvider<FixerAPI>()
    }

    func getExchange(currency: ExchangeCurrency) -> Observable<Exchange> {
        return self.provider.rx.request(.latest(currency: currency, symbols: [currency]))
            .map(Exchange.self)
            .asObservable()
    }

}
