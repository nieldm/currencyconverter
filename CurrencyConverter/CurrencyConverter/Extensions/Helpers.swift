import UIKit

infix operator ~: TernaryPrecedence
func ~<T> ( lhs: @autoclosure () -> T, rhs: @autoclosure () -> T) -> T {
    if UIDevice.current.userInterfaceIdiom != .pad {
        return lhs()
    }
    return rhs()
}
