import UIKit

extension String {
    var isNumeric: Bool {
        let predicate = NSPredicate(format:"SELF MATCHES '[0-9]+'")
        return predicate.evaluate(with:self.trimmingCharacters(in: [" "]))
    }
}
