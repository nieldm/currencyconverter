import UIKit

enum CopernicusFontWeight: String {
    case Book
    case BookItalic
}

extension UIFont {
    class func primary(_ size: CGFloat, weight: CopernicusFontWeight = .Book) -> UIFont {
        return UIFont(name: "Copernicus-\(weight)", size: size)!
    }
    
    class func secondary(_ size: CGFloat) -> UIFont {
        return UIFont(name: "HUGE-AvantGarde-Bold", size: size)!
    }
}
