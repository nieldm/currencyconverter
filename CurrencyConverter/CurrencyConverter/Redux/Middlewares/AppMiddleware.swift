import Foundation
import RxSwift
import Moya

class AppMiddleware {

    func getActionObservable(payload: MainStore.Payload) -> Observable<MainStore.Payload?> {
        let (state, action) = payload
        switch action {
        case AppAction.changeExchange(let currency):
            let provider = exchangeModelFixerAPI
            return ExchangeModel(api: provider).get(from: currency)
                .debug("🐉")
                .map({ (exchange: Exchange) -> MainStore.Payload? in
                    return (state, AppPrivateAction.setExchange(exchange))
                })
        default: return Observable<MainStore.Payload?>.of((state, action))
        }
    }

}
