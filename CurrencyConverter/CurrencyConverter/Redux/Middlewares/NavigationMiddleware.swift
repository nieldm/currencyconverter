import Foundation
import RxSwift
import Moya

class NavigationMiddleware {

    func getActionObservable(payload: MainStore.Payload) -> Observable<MainStore.Payload?> {
        let (state, action) = payload
        switch action {
        case AppAction.showExchange:
            store.dispatch(action: AppAction.changeExchange(.usd))
            let vc = ExchangesViewController()
            DispatchQueue.main.async {
                globalNavigationController?.pushViewController(vc, animated: true)
            }
            return Observable<MainStore.Payload?>.of((state, action))
        case AppAction.showRequestInput:
            let vc = RequestInputViewController()
            DispatchQueue.main.async {
                globalNavigationController?.pushViewController(vc, animated: true)
            }
            return Observable<MainStore.Payload?>.of((state, action))
        default: return Observable<MainStore.Payload?>.of((state, action))
        }
    }

}

