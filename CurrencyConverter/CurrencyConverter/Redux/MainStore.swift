import Foundation
import RxSwift

fileprivate let appReducer = AppReducer()
fileprivate let appMiddleware = AppMiddleware()
fileprivate let navigationMiddleware = NavigationMiddleware()
fileprivate let initialState = AppState.create(amount: 0)

let store = MainStore(initialState: initialState,
                      reducer: appReducer.getActionObservable,
                      middlewares: [appMiddleware.getActionObservable, navigationMiddleware.getActionObservable])

class MainStore: Store<AppState> {

}

extension Reactive where Base: MainStore {

    var observable: Observable<AppState> {
        get {
            return base.state.asObservable().share()
        }
    }

}
