import Foundation

enum AppAction: Action {
    case increaseAmount
    case decreaseAmount
    case setAmount(Int)
    case changeExchange(ExchangeCurrency)
    case showExchange
    case showRequestInput
    case show(ExchangeCurrency)
}

enum AppPrivateAction: Action {
    case setExchange(Exchange)
}
