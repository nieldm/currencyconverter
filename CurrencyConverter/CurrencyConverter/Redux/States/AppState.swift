import Then

struct AppState: Then, Equatable {
    var amount: Int = 0
    var exchange: ExchangeCurrency?
    var rates: [String: Float]?
    var totals: [String: Float]?
    var favorites: [ExchangeCurrency] = []
    var favoriteSelected: ExchangeCurrency?

    static func ==(lhs: AppState, rhs: AppState) -> Bool {
        return lhs.amount == rhs.amount
    }
}

extension AppState {
    static func create(amount: Int) -> AppState {
        let favorites = [
            ExchangeCurrency.brl,
            ExchangeCurrency.eur,
            ExchangeCurrency.gbp,
            ExchangeCurrency.jpy
        ]
        return AppState(
            amount: amount,
            exchange: nil,
            rates: nil,
            totals: nil,
            favorites: favorites,
            favoriteSelected: nil
        )
    }
}
