import XCTest
import RxTest
import RxBlocking
@testable import CurrencyConverter

class AppReducerTest: XCTestCase {
    
    func testIncreaseAmount() {
        let sut = AppReducer()
        let initialState = AppState.create(amount: 0)
        let lastValue = try! sut.getActionObservable(payload: (initialState, AppAction.increaseAmount))
            .toBlocking()
            .last()
        XCTAssertEqual(lastValue?.amount, 1)
    }

    func testIncreaseAmountOverflow() {
        let sut = AppReducer()
        let initialState = AppState.create(amount: Int.max)
        let lastValue = try! sut.getActionObservable(payload: (initialState, AppAction.increaseAmount))
            .toBlocking()
            .last()
        XCTAssertEqual(lastValue?.amount, Int.max)
    }

    func testDecreaseAmount() {
        let sut = AppReducer()
        let initialState = AppState.create(amount: 1)
        let lastValue = try! sut.getActionObservable(payload: (initialState, AppAction.decreaseAmount))
            .toBlocking()
            .last()
        XCTAssertEqual(lastValue?.amount, 0)
    }

    func testDecreaseAmountBelowZero() {
        let sut = AppReducer()
        let initialState = AppState.create(amount: 0)
        let lastValue = try! sut.getActionObservable(payload: (initialState, AppAction.decreaseAmount))
            .toBlocking()
            .last()
        XCTAssertEqual(lastValue?.amount, 0)
    }

    func testSetAmount() {
        let sut = AppReducer()
        let initialState = AppState.create(amount: 0)
        let lastValue = try! sut.getActionObservable(payload: (initialState, AppAction.setAmount(42)))
            .toBlocking()
            .last()
        XCTAssertEqual(lastValue?.amount, 42)
    }

    func testExchange() {
        let sut = AppReducer()
        let initialState = AppState.create(amount: 20)
        let jsonString = """
        {
            "base": "USD",
            "date": "2018-05-22",
            "rates": {
                "BRL": 3.0
            }
        }
        """
        if let jsonData = jsonString.data(using: .utf8),
            let exchange = try? JSONDecoder().decode(Exchange.self, from: jsonData)
        {
            let lastValue = try! sut.getActionObservable(payload: (initialState, AppPrivateAction.setExchange(exchange)))
                .toBlocking()
                .last()
            XCTAssertEqual(lastValue?.totals?["BRL"], 60)
        } else {
            XCTFail("Can't parse")
        }

    }

}
