import Foundation
import RxSwift

class AppReducer {

    func getActionObservable(payload: MainStore.Payload) -> Observable<AppState> {
        return Observable.of(payload)
            .map {
                let (state, action) = $0
                switch action {
                case AppAction.increaseAmount:
                    guard state.amount < Int.max else {
                        return state
                    }
                    return state.with {
                        $0.amount += 1
                        $0.totals = calculateTotals(state: $0)
                    }
                case AppAction.decreaseAmount:
                    return state.with {
                        $0.amount = max(0, $0.amount - 1)
                        $0.totals = calculateTotals(state: $0)
                    }
                case AppAction.setAmount(let amount):
                    return state.with {
                        $0.amount = amount
                        $0.totals = calculateTotals(state: $0)
                    }
                case AppPrivateAction.setExchange(let exchange):
                    return state.with {
                        $0.exchange = exchange.base
                        $0.rates = exchange.rates
                        $0.totals = calculateTotals(state: $0)
                        $0.favoriteSelected = ExchangeCurrency.brl
                    }
                case AppAction.show(let exchange):
                    return state.with {
                        $0.favoriteSelected = exchange
                    }
                default:
                    return state
                }
        }
    }
}

fileprivate func calculateTotals(state: AppState) -> [String: Float] {
    guard let rates = state.rates else {
        return [:]
    }
    return Dictionary(uniqueKeysWithValues:
        rates.map { key, value in (key.uppercased(), value * Float(state.amount)) })
}
