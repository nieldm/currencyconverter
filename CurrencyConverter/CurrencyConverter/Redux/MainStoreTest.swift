import XCTest
import RxTest
import RxCocoa
@testable import CurrencyConverter

class MainStoreTest: XCTestCase {

    private var observer: TestableObserver<AppState>!

    override func setUp() {
        super.setUp()

        let scheduler = TestScheduler(initialClock: 0)
        observer = scheduler.createObserver(AppState.self)
        scheduler.start()
    }

    func testExchange() {
        let sut = store
        let _ = sut.state.asObservable()
            .debug("Test Observer")
            .subscribe(self.observer)
        sut.dispatch(action: AppAction.setAmount(20))

        let correctValues = [
            next(0, AppState.create(amount: 0)),
            next(0, AppState.create(amount: 20))
        ]

        XCTAssertEqual(observer.events, correctValues)
    }
    
}
