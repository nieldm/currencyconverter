import XCTest
import RxSwift
import RxTest
import RxBlocking
import RxSwiftExt
import Then
@testable import CurrencyConverter

struct TestState: Then, Equatable {
    var count: Int = 0

    static func ==(lhs: TestState, rhs: TestState) -> Bool {
        return lhs.count == rhs.count
    }
}

enum TestAction: Action {
    case test
}

class TestMiddleware {

    func getActionObservable(payload: Store<TestState>.Payload) -> Observable<Store<TestState>.Payload?> {
        let (state, action) = payload
        switch action {
        default: return Observable<Store<TestState>.Payload?>.of((state, action))
        }
    }

}

class TestReducer {

    func getActionObservable(payload: Store<TestState>.Payload) -> Observable<TestState> {
        return Observable.of(payload)
            .map {
                let (state, action) = $0
                switch action {
                case TestAction.test:
                    return state.with {
                        $0.count = $0.count + 1
                    }
                default:
                    return state
                }
            }
    }
}

class StoreTest: XCTestCase {

    private var observer: TestableObserver<TestState>!

    override func setUp() {
        super.setUp()

        let scheduler = TestScheduler(initialClock: 0)
        observer = scheduler.createObserver(TestState.self)
        scheduler.start()
    }

    func testStoreStates() {
        let testReducer = TestReducer()
        let testMiddleware = TestMiddleware()
        let sut = Store(initialState: TestState(), reducer: testReducer.getActionObservable, middlewares: [testMiddleware.getActionObservable])

        let _ = sut.state.asObservable()
            .debug("Test Observer")
            .subscribe(self.observer)

        sut.dispatch(action: TestAction.test)
        sut.dispatch(action: TestAction.test)

        let correctValues = [
            next(0, TestState(count: 0)),
            next(0, TestState(count: 1)),
            next(0, TestState(count: 2))
        ]

        XCTAssertEqual(observer.events, correctValues)
    }
}
