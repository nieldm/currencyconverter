//
//  Redux.swift
//  CurrencyConverter
//
//  Created by nieldm on 5/19/18.
//  Copyright © 2018 nieldm. All rights reserved.
//

import Foundation
import RxSwift
import RxSwiftExt

public protocol Action { }
public protocol State {
    associatedtype ValueType
}

enum PrivateAction: Action {
    case boot
}

public typealias Reducer<ValueType: Equatable> = (_ payload: Store<ValueType>.Payload) -> Observable<ValueType>
public typealias Middleware<ValueType: Equatable> = (_ payload: Store<ValueType>.Payload) -> Observable<Store<ValueType>.Payload?>

public class Store<State: Equatable>: ReactiveCompatible {

    var state: Variable<State>

    public typealias StoreMiddleware = Middleware<State>
    public typealias StoreReducer = Reducer<State>
    public typealias Payload = (State, Action)

    private var actions: Variable<Action>
    private var disposeBag: DisposeBag = DisposeBag()
    private var middlewares: [StoreMiddleware]
    private var reducer: StoreReducer

    init(initialState: State, reducer: @escaping StoreReducer, middlewares: [StoreMiddleware]) {
        self.state = Variable<State>(initialState)
        self.actions = Variable<Action>(PrivateAction.boot)
        self.middlewares = middlewares
        self.reducer = reducer
        self.bind()
    }

    private func bind() {
        let actionObservable = self.actions.asObservable()
        actionObservable.map { (action) -> Payload in
                return (self.state.value, action)
            }
            .debug("🐉 Action Observable")
            .flatMap { (payload: Payload) -> Observable<Payload?> in
                return self.middleware(payload: payload)
            }
            .flatMap { (payload: Payload?) -> Observable<State> in
                guard let payload = payload else {
                    return self.state.asObservable()
                }
                return self.reducer(payload)
            }
            .subscribe(onNext: { (state: State) in
                self.state.value = state
            })
            .disposed(by: self.disposeBag)
    }

    private func middleware(payload: Payload) -> Observable<Payload?> {
        return Observable.create({ (observer) -> Disposable in
            return self.middlewares.reduce(Observable<Payload?>.of(payload), { (result: Observable<Payload?>, middlewareConcat: Middleware) -> Observable<Payload?> in
                return Store.concat(observer: result, middleware: middlewareConcat)
            })
                .debug("🐉 Middlewares")
                .subscribe(observer)
        })
    }

    public func dispatch(action: Action) {
        self.actions.value = action
    }

    fileprivate static func concat(observer: Observable<Payload?>, middleware: @escaping StoreMiddleware) -> Observable<Payload?> {
        return observer
            .unwrap()
            .flatMap(middleware)
    }

}
