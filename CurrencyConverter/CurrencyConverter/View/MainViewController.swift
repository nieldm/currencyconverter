import UIKit
import Then
import SnapKit

final class MainViewController: UIViewController {
    
    //MARK: Outlets
    private weak var logo: UIImageView!
    private weak var contentView: UIView!
    private weak var mainTitle: UILabel!
    
    //MARK: View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        buildView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        animateBackground()
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK: View Hierarchy
    private func buildView() {
        let mainView = UIView()
        
        view.addSubview(mainView)
        
        mainView.do {
            $0.backgroundColor = .white
            $0.snp.makeConstraints { make in
                make.size.equalTo(self.view)
                make.center.equalTo(self.view)
            }
        }
        
        let contentView = UIView()
        
        mainView.addSubview(contentView)
        
        contentView.do {
            $0.layer.opacity = 0
            $0.backgroundColor = UIColor.primary()
            $0.snp.makeConstraints { make in
                make.center.equalTo(mainView)
                make.size.equalTo(mainView)
            }
        }
        
        self.contentView = contentView
        
        let label = UILabel()
        
        contentView.addSubview(label)
        
        label.do {
            $0.text = "Currency\nConverter"
            $0.layer.opacity = 0
            $0.textColor = UIColor.white
            $0.backgroundColor = UIColor.clear
            $0.font = UIFont.secondary(62 ~ 82)
            $0.numberOfLines = 0
            $0.textAlignment = .center
            $0.snp.makeConstraints { make in
                make.center.equalTo(contentView)
                make.width.equalTo(contentView).multipliedBy(0.9)
            }
        }
        
        mainTitle = label
    }
    
    //MARK: Behavior
    private func startTimer() {
        let _ = Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(moveToNextView), userInfo: nil, repeats: false)
    }
    
    private func animateBackground() {
        let enterAnimation = CABasicAnimation(keyPath: "opacity").then {
            $0.fromValue = 0.0
            $0.toValue = 1.0
            $0.duration = 1.0
            $0.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)
            $0.beginTime = CACurrentMediaTime() + 0.7
            $0.fillMode = kCAFillModeBackwards
            $0.isRemovedOnCompletion = true
        }
        CATransaction.begin()
        CATransaction.setCompletionBlock {
            self.startTimer()
        }
        
        contentView?.layer.add(enterAnimation, forKey: nil)
        contentView?.layer.opacity = 1
        
        enterAnimation.beginTime = CACurrentMediaTime() + 0.45
        
        mainTitle?.layer.add(enterAnimation, forKey: nil)
        mainTitle?.layer.opacity = 1
        
        CATransaction.commit()
    }
    
    @objc func moveToNextView() {
        store.dispatch(action: AppAction.showRequestInput)
    }

}
