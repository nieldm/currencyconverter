import RxSwift
import RxCocoa
import UIKit

private enum BuckButton: Int {
    case Plus = 0, Minus
}

//MARK: Config Values
private let topAmountForSize: CGFloat = 1000
private let minFontSize: CGFloat = 24 ~ 62
private let maxFontSize: CGFloat = 92 ~ 122
private let firstStepDelay: Double = 3
private let secondStepDelay: Double = 6

final class RequestInputViewController: UIViewController {

    //MARK: Outlets
    private weak var bucksTextInput: UITextField!
    private weak var titleLabel: UILabel!
    private weak var minusButton: UIButton!
    private weak var plusButton: UIButton!
    private weak var continueButton: UIButton!

    private var disposeBag = DisposeBag()
    private var longPressTimer: Timer?
    private var longPressTimerStepOne: Timer?
    private var longPressTimerStepTwo: Timer?
    private var timerInterval = 0.5
    private var action: Selector?

    //MARK: View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        buildView()
        rxBind()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        titleLabel?.becomeFirstResponder()
    }
    
    //MARK: View Hierarchy
    private func buildView() {
        let mainView = UIView()
        
        view.addSubview(mainView)
        
        mainView.do {
            $0.backgroundColor = .white
            $0.snp.makeConstraints { make in
                make.size.equalTo(self.view)
                make.center.equalTo(self.view)
            }
        }
        
        let titleLabel = UILabel()
        
        mainView.addSubview(titleLabel)
        
        titleLabel.do {
            $0.numberOfLines = 0
            $0.font = .secondary(42 ~ 62)
            $0.text = "How many bucks do you have?"
            $0.textAlignment = .left ~ .center
            $0.snp.makeConstraints { make in
                make.centerX.equalTo(mainView)
                make.top.equalTo(mainView.safeAreaLayoutGuide).offset(35 ~ 75)
                make.width.equalTo(mainView).multipliedBy(0.9)
            }
        }
        
        self.titleLabel = titleLabel
        
        let bucksTextInput = UITextField()
        
        mainView.addSubview(bucksTextInput)
        
        bucksTextInput.do {
            $0.font = .primary(minFontSize)
            $0.addTarget(self, action: #selector(textFieldDidChangeText(textField:)), for: UIControlEvents.editingChanged)
            $0.delegate = self
            $0.tintColor = .primary()
            $0.keyboardType = .numberPad
            $0.textAlignment = .center
            $0.adjustsFontSizeToFitWidth = true
            $0.minimumFontSize = 10
            $0.snp.makeConstraints { make in
                make.center.equalTo(mainView)
                make.width.equalTo(mainView).multipliedBy(0.9)
            }
        }
        
        self.bucksTextInput = bucksTextInput
        
        let continueButton = UIButton()
        
        mainView.addSubview(continueButton)
        
        continueButton.do {
            $0.setTitle("Show me those rates", for: .normal)
            $0.titleLabel?.font = .primary(22 ~ 42)
            $0.setTitleColor(.primary(), for: .normal)
            $0.setTitleColor(.secondary(), for: .highlighted)
            $0.backgroundColor = .clear
            $0.layer.cornerRadius = 4
            $0.addTarget(self, action: #selector(continuePressed), for: .touchUpInside)
            $0.snp.makeConstraints { make in
                make.centerX.equalTo(mainView)
                make.width.equalTo(mainView).multipliedBy(0.8)
                make.height.equalTo(48 ~ 64)
                make.bottom.equalTo(mainView.safeAreaLayoutGuide).inset(15)
            }
        }
        
        self.continueButton = continueButton
        
        let minusButton = UIButton()
        
        mainView.addSubview(minusButton)
        
        let plusButton = UIButton()
        
        mainView.addSubview(plusButton)
        
        minusButton.do {
            $0.setImage(
                UIImage(named: "minus")?.withRenderingMode(.alwaysTemplate),
                for: .normal)
            $0.imageView?.tintColor = .white
            $0.titleLabel?.font = .primary(42 ~ 62)
            $0.setTitleColor(.white, for: .normal)
            $0.setTitleColor(.secondary(), for: .highlighted)
            $0.backgroundColor = .primary()
            $0.tag = BuckButton.Minus.rawValue
            $0.layer.cornerRadius = 4
            $0.addTarget(self, action: #selector(buttonPressed(button:)), for: .touchUpInside)
            $0.addTarget(self, action: #selector(longPressButton(button:)), for: .touchDown)
            $0.snp.makeConstraints { make in
                make.leading.equalTo(bucksTextInput.snp.leading).offset(15)
                make.top.equalTo(bucksTextInput.snp.bottom).offset(15)
                make.height.equalTo(48 ~ 84)
                make.width.equalTo(plusButton)
                make.trailing.equalTo(mainView.snp.centerXWithinMargins).offset(-4)
            }
        }
        
        plusButton.do {
            $0.setImage(
                UIImage(named: "plus")?.withRenderingMode(.alwaysTemplate),
                for: .normal)
            $0.tintColor = .white
            $0.titleLabel?.font = .primary(42 ~ 62)
            $0.setTitleColor(.white, for: .normal)
            $0.setTitleColor(.secondary(), for: .highlighted)
            $0.backgroundColor = .primary()
            $0.layer.cornerRadius = 4
            $0.tag = BuckButton.Plus.rawValue
            $0.addTarget(self, action: #selector(buttonPressed(button:)), for: .touchUpInside)
            $0.addTarget(self, action: #selector(longPressButton(button:)), for: .touchDown)
            $0.snp.makeConstraints { make in
                make.trailing.equalTo(bucksTextInput.snp.trailing).inset(15)
                make.top.equalTo(bucksTextInput.snp.bottom).offset(15)
                make.height.equalTo(48 ~ 84)
                make.width.equalTo(minusButton)
                make.leading.equalTo(mainView.snp.centerXWithinMargins).offset(4)
            }
        }
        
        self.minusButton = minusButton
        self.plusButton = plusButton
    }
    
    //MARK: Behavior

    private func rxBind() {
        store.rx.observable
            .map { $0.amount }
            .distinctUntilChanged()
            .asDriver(onErrorJustReturn: 0)
            .drive(onNext: { amount in
                self.modelDidUpdate(model: amount)
            }).disposed(by: self.disposeBag)
    }

    @objc func textFieldDidChangeText(textField: UITextField) {
        guard let text = textField.text,
            let value = Int(text)
            else { return }
        store.dispatch(action: AppAction.setAmount(value))
    }

    private func modelDidUpdate(model: Int) {
        bucksTextInput.text = "\(model)"
        updateFontSize(model: model)
        bounce(view: bucksTextInput)
    }
    
    private func updateFontSize(model: Int) {
        let progress: CGFloat = CGFloat(model) / topAmountForSize,
            maxFont: CGFloat  = maxFontSize,
            minFont: CGFloat  = minFontSize,
            size = min(max(minFont, maxFont * progress), maxFont)
        bucksTextInput.font = UIFont.primary(size)
    }
    
    @objc func buttonPressed(button: UIButton) {
        defer { bounce(view: button) }
        longPressTimer?.invalidate()
        longPressTimer = nil
        longPressTimerStepOne?.invalidate()
        longPressTimerStepOne = nil
        longPressTimerStepTwo?.invalidate()
        longPressTimerStepTwo = nil
        guard let buckButton = BuckButton(rawValue: button.tag) else { return }
        switch buckButton {
        case .Plus:
            plus()
            return
        case .Minus:
            minus()
            return
        }
    }
    
    func getAmount() -> Int {
        guard let text = bucksTextInput.text,
            let amount = Int(text)
            else { return 0 }
        return amount
    }
    
    @objc func continuePressed() {
        store.dispatch(action: AppAction.showExchange)
    }
    
    @objc func minus() {
        store.dispatch(action: AppAction.decreaseAmount)
    }
    
    
    @objc func plus() {
        store.dispatch(action: AppAction.increaseAmount)
    }
    
    private func bounce(view: UIView?) {
        guard let view = view else { return }
        let origin:CGPoint = view.center
        let target:CGPoint = CGPoint(x: view.center.x, y: view.center.y + 4)
        let bounce = CABasicAnimation(keyPath: "position.y")
        bounce.duration = 0.1
        bounce.fromValue = origin.y
        bounce.toValue = target.y
        bounce.repeatCount = 1
        bounce.autoreverses = true
        bounce.isRemovedOnCompletion = true
        view.layer.add(bounce, forKey: nil)
    }
    
    @objc func longPressButton(button: UIButton) {
        bucksTextInput.resignFirstResponder()
        guard let buckButton = BuckButton(rawValue: button.tag) else { return }
        switch buckButton {
        case .Plus:
            createTimer(action: #selector(plus))
            return
        case .Minus:
            createTimer(action: #selector(minus))
            return
        }
    }
    
    private func createTimer(action: Selector) {
        self.action = action
        changeTimer(interval: timerInterval, action: action)
        longPressTimerStepOne = Timer.scheduledTimer(timeInterval: firstStepDelay, target: self, selector: #selector(increaseTimerSpeedStepOne), userInfo: nil, repeats: false)
        longPressTimerStepTwo = Timer.scheduledTimer(timeInterval: secondStepDelay, target: self, selector: #selector(increaseTimerSpeedStepTwo), userInfo: nil, repeats: false)
    }
    
    private func changeTimer(interval: Double, action: Selector) {
        longPressTimer?.invalidate()
        longPressTimer = nil
        longPressTimer = Timer.scheduledTimer(timeInterval: interval, target: self, selector: action, userInfo: nil, repeats: true)
    }
    
    @objc func increaseTimerSpeedStepOne() {
        guard let action = action else { return }
        changeTimer(interval: timerInterval / 4, action: action)
    }
    
    @objc func increaseTimerSpeedStepTwo() {
        guard let action = action else { return }
        changeTimer(interval: timerInterval / 8, action: action)
    }
}

//MARK: UITextFieldDelegate
extension RequestInputViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard !string.isEmpty else { return true }
        guard let finalText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
            else { return false }
        return finalText.isNumeric
    }
}
