import Then
import SnapKit
import UIKit
import RxDataSources

struct ExchangeViewCellData {
    var currency: String
    var rate: Float
    var total: Float
}

struct SectionOfCustomData {
    var header: String
    var items: [Item]
}
extension SectionOfCustomData: SectionModelType {
    typealias Item = ExchangeViewCellData
    
    init(original: SectionOfCustomData, items: [Item]) {
        self = original
        self.items = items
    }
}

class ExchangeViewCell: UICollectionViewCell {

    private weak var currencyNameLabel: UILabel!
    private weak var currencyRateLabel: UILabel!
    private weak var currencyTotalLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    //MARK: View Hierarchy
    private func buildView() {
        let mainView = UIView()
        
        contentView.addSubview(mainView)
        
        mainView.do {
            $0.backgroundColor = .clear
            $0.layer.borderColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.25).cgColor
            $0.layer.borderWidth = 10
            $0.layer.cornerRadius = 8
            $0.snp.makeConstraints { make in
                make.size.equalTo(self.contentView)
                make.center.equalTo(self.contentView)
            }
        }
        
        let currencyNameLabel = UILabel()
        
        mainView.addSubview(currencyNameLabel)
        
        currencyNameLabel.do {
            $0.font = .secondary(42 ~ 72)
            $0.text = "BITCOIN"
            $0.textAlignment = .center
            $0.snp.makeConstraints { make in
                make.top.equalTo(mainView.snp.top).offset(45)
                make.centerX.equalTo(mainView)
                make.width.equalTo(mainView).multipliedBy(0.8)
            }
        }
        
        self.currencyNameLabel = currencyNameLabel
        
        let currencyNameSubTitleLabel = UILabel()
        
        mainView.addSubview(currencyNameSubTitleLabel)
        
        currencyNameSubTitleLabel.do {
            $0.textColor = .black
            $0.font = .primary(12 ~ 24)
            $0.text = "currency"
            $0.textAlignment = .center
            $0.snp.makeConstraints { make in
                make.top.equalTo(currencyNameLabel.snp.bottom).offset(5)
                make.centerX.equalTo(mainView)
                make.width.equalTo(mainView).multipliedBy(0.8)
            }
        }
        
        let containerView = UIView()
        
        mainView.addSubview(containerView)
        
        containerView.do {
            $0.snp.makeConstraints { make in
                make.height.equalTo(mainView).multipliedBy(0.6)
                make.width.equalTo(mainView)
                make.centerX.equalTo(mainView)
                make.bottom.equalTo(mainView.snp.bottom)
            }
        }
        
        let currencyTotalLabel = UILabel()
        
        containerView.addSubview(currencyTotalLabel)
        
        currencyTotalLabel.do {
            $0.font = .secondary(62 ~ 82)
            $0.text = "$$$$$"
            $0.textAlignment = .center
            $0.adjustsFontSizeToFitWidth = true
            $0.snp.makeConstraints { make in
                make.top.equalTo(containerView.snp.centerY)
                make.centerX.equalTo(containerView)
                make.width.equalTo(containerView).multipliedBy(0.8)
            }
        }
        
        self.currencyTotalLabel = currencyTotalLabel
        
        let currencyRateLabel = UILabel()
        
        containerView.addSubview(currencyRateLabel)
        
        currencyRateLabel.do {
            $0.textAlignment = .center
            $0.font = .primary(12 ~ 24)
            $0.text = "total"
            $0.snp.makeConstraints { make in
                make.top.equalTo(currencyTotalLabel.snp.bottom)
                make.centerX.equalTo(containerView)
                make.width.equalTo(containerView).multipliedBy(0.8)
            }
        }
        
        self.currencyRateLabel = currencyRateLabel
    }
    
    func set(data: ExchangeViewCellData) {
        currencyNameLabel?.text = data.currency
        currencyRateLabel?.text = "rate: \(data.rate)"
        currencyTotalLabel?.text = "\(data.total)"
    }

}
