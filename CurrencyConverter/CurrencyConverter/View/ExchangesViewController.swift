import Then
import SnapKit
import UIKit
import RxSwift
import RxCocoa
import RxDataSources

private enum ZPositionOrder: CGFloat {
    case Bottom = 1, Middle, Top
}

private enum CitiesBGs: String {
    case EUR, GBP, BRL
}

final class ExchangesViewController: UIViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private weak var collectionView: UICollectionView! {
        didSet {
            collectionView.do {
                $0.backgroundColor = .clear
                $0.layer.cornerRadius = 8
                $0.layer.zPosition = ZPositionOrder.Middle.rawValue
                self.configure(collectionView: $0)
            }
        }
    }
    private weak var amountLabel: UILabel! {
        didSet {
            amountLabel.do {
                $0.text = "\(amount)"
                $0.font = .secondary(82 ~ 102)
                $0.textColor = .white
                $0.textAlignment = .center
                $0.adjustsFontSizeToFitWidth = true
            }
        }
    }
    private weak var amountSubTitleLabel: UILabel! {
        didSet {
            amountSubTitleLabel.do {
                $0.textAlignment = .center
                $0.textColor = .white
                $0.font = .primary(12 ~ 24)
                $0.text = "bucks"
            }
        }
    }
    
    private weak var imageContainer: UIView! {
        didSet {
            imageContainer.do {
                let layer = CAShapeLayer()
                $0.backgroundColor = .clear
                $0.layer.zPosition = ZPositionOrder.Top.rawValue
                $0.layer.addSublayer(layer)
                imageLayer = layer
            }
        }
    }
    
    private var favoritesButtons: [UIButton]! {
        didSet {
            favoritesButtons.forEach {
                $0.do {
                    $0.setTitle("", for: .normal)
                    $0.titleLabel?.font = .primary(24 ~ 36)
                    $0.setTitleColor(.primary(), for: .normal)
                    $0.setTitleColor(.secondary(), for: .highlighted)
                    $0.backgroundColor = .white
                    $0.layer.cornerRadius = 4
                    $0.isHidden = true
                    $0.addTarget(self, action: #selector(favoritePressed(button:)), for: .touchUpInside)
                }
            }
        }
    }
    
    private weak var changeButton: UIButton! {
        didSet {
            changeButton.do {
                $0.setTitle("Edit my bucks", for: .normal)
                $0.titleLabel?.font = .primary(22 ~ 42)
                $0.setTitleColor(.black, for: .normal)
                $0.backgroundColor = .white
                $0.layer.zPosition = ZPositionOrder.Top.rawValue
                $0.addTarget(self, action: #selector(changePressed), for: .touchUpInside)
                $0.layer.cornerRadius = 5
            }
        }
    }

    private var disposeBag = DisposeBag()
    private var imageLayer: CAShapeLayer?
    private var amount: Int
    private var data = Variable<[SectionOfCustomData]>([])
    private var favData: [ExchangeCurrency] = []
    
    init() {
        self.amount = 0
        super.init(nibName: nil, bundle: nil)
        buildView()
        style()
        rxBind()
    }
    
    required init?(coder aDecoder: NSCoder) {
        amount = 0
        super.init(coder: aDecoder)
    }
    
    //MARK: View Hierarchy
    private func buildView() {
        
        let layout = UICollectionViewLayout()
        
        let _ = UIView().then {
            self.view.addSubview($0)
            $0.snp.makeConstraints { make in
                make.width.equalTo(view)
                make.bottom.equalTo(view)
                make.height.equalTo(view.safeAreaLayoutGuide).multipliedBy(0.55 ~ 0.5)
                make.centerX.equalTo(view)
            }
            self.imageContainer = $0
        }
        
        let collectionView = UICollectionView(frame: CGRect(), collectionViewLayout: layout)
            .then {
            view.addSubview($0)
            $0.snp.makeConstraints { make in
                make.height.equalTo(view.safeAreaLayoutGuide).multipliedBy(0.8)
                make.width.equalTo(view.safeAreaLayoutGuide).multipliedBy(0.85)
                make.center.equalTo(view)
            }
            self.collectionView = $0
        }
        
        let amountLabel = UILabel()
            .then {
            view.addSubview($0)
            $0.clipsToBounds = false
            $0.snp.makeConstraints { make in
                make.center.equalTo(collectionView)
                make.width.equalTo(collectionView)
            }
            self.amountLabel = $0
        }
        
        let _ = UIButton()
            .then {
            view.addSubview($0)
            $0.snp.makeConstraints { make in
                make.height.equalTo(45 ~ 68)
                make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom).offset(5)
                make.width.equalTo(view).multipliedBy(0.85)
                make.centerX.equalTo(view)
            }
            self.changeButton = $0
        }
        
        let _ = UILabel()
            .then {
            view.addSubview($0)
            $0.snp.makeConstraints { make in
                make.top.equalTo(amountLabel.snp.bottom).inset(5)
                make.width.equalTo(amountLabel)
                make.centerX.equalTo(amountLabel)
            }
            self.amountSubTitleLabel = $0
        }
        
        var lastButton: UIButton?
        let numOfButtons = 4
        self.favoritesButtons = []
        for pos in 1...numOfButtons {
            let _ = UIButton().then {
                view.addSubview($0)
                $0.snp.makeConstraints { make in
                    make.height.equalTo(35 ~ 65)
                    if let lastButton = lastButton {
                        make.left.equalTo(lastButton.snp.right).offset(5)
                        make.width.equalTo(lastButton)
                    } else {
                        make.left.equalTo(view).offset(10)
                    }
                    make.top.equalTo(view.safeAreaLayoutGuide).offset(5)
                    if pos == numOfButtons {
                        make.right.equalTo(view).inset(10)
                    }
                }
                self.favoritesButtons.append($0)
                lastButton = $0
            }
        }
    }
    
    private func style() {
        view.do {
            $0.backgroundColor = .primary()
        }
    }
    
    private func addBackgroundAnimation(path: (CGRect) -> UIBezierPath) {
        guard let layer = imageLayer else { return }
        layer.path = path(imageContainer.bounds).cgPath
        layer.strokeColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.25).cgColor
        layer.lineWidth = 3
        layer.fillColor = UIColor.white.cgColor
        
        let pathAnimation = CABasicAnimation(keyPath: "strokeEnd")
        pathAnimation.duration = 5.0
        pathAnimation.fromValue = 0.0
        pathAnimation.toValue = 1.0
        
        let fillAnimation = CABasicAnimation(keyPath: "fillColor")
        fillAnimation.duration = 8.0
        fillAnimation.fromValue = UIColor.clear.cgColor
        fillAnimation.toValue = UIColor.white.cgColor
        
        layer.add(pathAnimation, forKey: nil)
        layer.add(fillAnimation, forKey: nil)
    }

    private func changeImageContainerVisibility(show: Bool = true) {
        let hideAnimation = CABasicAnimation(keyPath: "opacity").then {
            $0.fromValue = imageLayer?.opacity
            $0.toValue = show ? 1.0 : 0.0
        }
        
        imageLayer?.add(hideAnimation, forKey: nil)
        imageLayer?.opacity = show ? 1.0 : 0.0
    }

    private func rxBind() {
        store.rx.observable
            .map { $0.amount }
            .distinctUntilChanged()
            .asDriver(onErrorJustReturn: 0)
            .drive(onNext: { amount in
                self.amountLabel.text = "\(amount)"
            }).disposed(by: self.disposeBag)
        
        store.rx.observable
            .map { $0.favorites }
            .asDriver(onErrorJustReturn: [])
            .drive(onNext: { favorites in
                self.favoritesDidChange(data: favorites)
            }).disposed(by: self.disposeBag)
        
        self.collectionView.rx
            .setDelegate(self)
            .disposed(by: self.disposeBag)
    
        let dataSource: RxCollectionViewSectionedReloadDataSource<SectionOfCustomData> = RxCollectionViewSectionedReloadDataSource<SectionOfCustomData>(configureCell: { (dataSource, collectionView, indexPath, data) -> UICollectionViewCell in
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? ExchangeViewCell
            cell?.set(data: data)
            return cell ?? UICollectionViewCell(frame: CGRect.zero)
        })

        store.rx.observable
            .map { (state: AppState) -> [SectionOfCustomData] in
                var result: [ExchangeViewCellData] = []
                state.rates?.forEach({ (arg) in
                    let (key, rate) = arg
                    if let total = state.totals?[key] {
                        result.append(ExchangeViewCellData(currency: key, rate: rate, total: total))
                    }
                })
                return [SectionOfCustomData(header: "Exchanges", items: result)]
            }
            .bind(to: data)
            .disposed(by: disposeBag)

        data.asObservable()
            .bind(to: self.collectionView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)

        store.rx.observable
            .map { $0.favoriteSelected }
            .unwrap()
            .asDriver(onErrorDriveWith: Driver.never())
            .drive(onNext: { favorite in
                guard let indexModel = self.findIndex(needle: favorite) else {
                    return
                }
                let indexPath = IndexPath(item: indexModel, section: 0)
                self.collectionView.scrollToItem(at: indexPath, at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
            })
            .disposed(by: disposeBag)

        self.collectionView.rx.didEndScrollingAnimation
            .subscribe(onNext: {
                guard let cell = self.collectionView.visibleCells.first else {
                    return
                }
                guard let indexPath = self.collectionView.indexPath(for: cell) else {
                    return
                }
                guard let model: ExchangeViewCellData = try? self.collectionView.rx.model(at: indexPath) else {
                    return
                }
                guard let currency = ExchangeCurrency(rawValue: model.currency) else {
                    return
                }
                self.mainDataDidChange(data: currency)
            })
            .disposed(by: disposeBag)
    }

    private func findIndex(needle: ExchangeCurrency) -> Int? {
        guard let values = self.data.value.first else {
            return nil
        }
        var index: Int?
        for (i, item) in values.items.enumerated() {
            if item.currency == needle.rawValue {
                index = i
            }
        }
        return index
    }

    private func configure(collectionView: UICollectionView) {
        collectionView.do {
            let flowLayout = UICollectionViewFlowLayout().then {
                $0.scrollDirection = .horizontal
                $0.footerReferenceSize = CGSize.zero
                $0.headerReferenceSize = CGSize.zero
                $0.sectionInset = UIEdgeInsets()
                $0.minimumInteritemSpacing = 0.0
                $0.minimumLineSpacing = 0.0
            }
            $0.setCollectionViewLayout(flowLayout, animated: false)
            $0.isPagingEnabled = true
            $0.register(ExchangeViewCell.self, forCellWithReuseIdentifier: "Cell")
            $0.reloadData()
        }
    }
    
    @objc func favoritePressed(button: UIButton) {
        changeImageContainerVisibility(show: false)
        let tag = button.tag
        if favData.count - 1 < tag { return }
        let fav = favData[tag]
        store.dispatch(action: AppAction.show(fav))
    }
    
    @objc func changePressed() {
        navigationController?.popViewController(animated: true)
    }
}

extension ExchangesViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(
            width: collectionView.bounds.width,
            height: collectionView.bounds.height)
    }
}

extension ExchangesViewController {
    func didCollectionViewBeginScrolling() {
        changeImageContainerVisibility(show: false)
    }

    func mainDataDidChange(data: ExchangeCurrency) {
        guard let bg = CitiesBGs(rawValue: data.rawValue) else {
            changeImageContainerVisibility(show: false)
            return
        }
        defer { changeImageContainerVisibility() }
        switch bg {
        case .BRL:
            addBackgroundAnimation(path: Path.getRio)
        case .GBP:
            addBackgroundAnimation(path: Path.getLondon)
        default:
            addBackgroundAnimation(path: Path.getBerlin)
        }
        return
    }

    func favoritesDidChange(data: [ExchangeCurrency]) {
        favData = data
        data.enumerated().forEach { key, fav in
            guard let favoritesButtons = favoritesButtons else { return }
            if favoritesButtons.count - 1 < key { return }
            favoritesButtons[key].do {
                $0.setTitle(fav.rawValue, for: .normal)
                $0.tag = key
                $0.isHidden = false
            }
        }
    }
}
